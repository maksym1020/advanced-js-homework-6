// Написати програму "Я тебе знайду по IP"
//
// #### Технічні вимоги:
// - Створити просту HTML-сторінку з кнопкою `Знайти по IP`.
// - Натиснувши кнопку - надіслати AJAX запит за адресою `https://api.ipify.org/?format=json`,
// отримати звідти IP адресу клієнта.
// - Дізнавшись IP адресу, надіслати запит на сервіс `https://ip-api.com/` та отримати
// інформацію про фізичну адресу.
// - під кнопкою вивести на сторінку інформацію, отриману з останнього запиту –
// континент, країна, регіон, місто, район.
// - Усі запити на сервер необхідно виконати за допомогою async await.
//
// ## Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

const root = document.querySelector('#root');
const checkBtn = document.querySelector('.btn-danger');

const ipCheckUrl = 'https://api.ipify.org/?format=json'
const ipInfoUrl = 'http://ip-api.com/json/'

class Request {
    async get(url, ip = '') {
        try {
            const response = await fetch(url + ip);
            return await response.json();
        } catch (error) {
            console.log(error);
        }
    }
}

class Card {
    render(obj) {

        let {query, timezone, country: objCountry, city: objCity, regionName, lat, lon, isp} = obj;

        checkBtn.classList.add('d-none');

        const template = document.querySelector('#card').content;
        const card = template.querySelector('.card');

        const ip = card.querySelector('.ip-address');
        const continent = card.querySelector('.continent');
        const country = card.querySelector('.country');
        const region = card.querySelector('.region');
        const city = card.querySelector('.city');
        const latitude = card.querySelector('.lat');
        const longitude = card.querySelector('.lon');
        const provider = card.querySelector('.provider');

        ip.textContent = query;
        continent.textContent = `${timezone.split('/')[0]}, `;
        country.textContent = objCountry;
        region.textContent = `Region: ${regionName}`;
        city.textContent = `City: ${objCity}`;
        latitude.textContent = `Latitude: ${lat}`;
        longitude.textContent = `Longitude: ${lon}`;
        provider.textContent = `Provider: ${isp}`;

        return card;
    }
}

const request = new Request();
const card = new Card();

checkBtn.addEventListener('click', async () => {
    try {
        const ipResponse = await request.get(ipCheckUrl);
        console.log(ipResponse);
        const ipInfoResponse = await request.get(ipInfoUrl, ipResponse.ip);
        root.append(card.render(ipInfoResponse));
    } catch (error) {
        console.log(error);
    }
})




